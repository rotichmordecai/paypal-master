<?php
include_once 'config/config.php';
include_once 'autoload.php';
include_once 'lib/lib.php';
include_once 'lib/database.php';

$database = new Database();
$records = ORM::for_table('customer')->order_by_asc('customer_last_payment')->find_many();
?>

<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Basic Page Needs
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta charset="utf-8">
        <title>PayPal: Customers on Contract</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- FONT
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link href='//fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'>

        <!-- CSS
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="stylesheet" href="dist/css/normalize.css">
        <link rel="stylesheet" href="dist/css/skeleton.css">
        <link rel="stylesheet" href="css/custom.css">

        <!-- Scripts
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
        <link rel="stylesheet" href="css/github-prettify-theme.css">
        <script src="js/site.js"></script>

        <!-- Favicon
        –––––––––––––––––––––––––––––––––––––––––––––––––– -->
        <link rel="icon" type="image/png" href="dist/images/favicon.png">

    </head>
    <body class="code-snippets-visible">
        <div class="container">
            <div class="docs-section" id="tables">
                <h6 class="docs-header">PayPal - Recurring Payments(CUSTOMERS)</h6>
                <p>Users with recurring payments</p>
                <table class="u-full-width" style="font-size: 13px;">
                    <thead>
                        <tr>
                            <th>PayPal ID</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Subscribed</th>
                            <th>Last Payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record): ?>
                            <tr>
                                <td><?php echo $record->customer_id ?></td>
                                <td><?php echo $record->customer_email ?></td>
                                <td><?php echo $record->customer_name ?></td>
                                <td><?php echo $record->customer_created ?></td>
                                <td><?php echo $record->customer_last_payment ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
