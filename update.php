<?php

ini_set('memory_limit', '-1');
set_time_limit(10800);

include_once 'config/config.php';
include_once 'autoload.php';
include_once 'lib/lib.php';
include_once 'lib/database.php';

$database = new Database();

$start_date = "2017-09-17";
$range = date_data($start_date, 'monthly');

foreach ($range as $value) {
    $TSFields = array(
        'startdate' => gmdate("Y-m-d\\TH:i:sZ", strtotime($value['date1'])), // Required.  The earliest transaction date you want returned.  Must be in UTC/GMT format.  2008-08-30T05:00:00.00Z
        'enddate' => gmdate("Y-m-d\\TH:i:sZ", strtotime($value['date2'])), // The latest transaction date you want to be included.
        'email' => '', // Search by the buyer's email address.
        'receiver' => '', // Search by the receiver's email address.
        'receiptid' => '', // Search by the PayPal account optional receipt ID.
        'transactionid' => '', // Search by the PayPal transaction ID.
        'invnum' => '', // Search by your custom invoice or tracking number.
        'acct' => '', // Search by a credit card number, as set by you in your original transaction.
        'auctionitemnumber' => '', // Search by auction item number.
        'transactionclass' => '', // Search by classification of transaction.  Possible values are: All, Sent, Received, MassPay, MoneyRequest, FundsAdded, FundsWithdrawn, Referral, Fee, Subscription, Dividend, Billpay, Refund, CurrencyConversions, BalanceTransfer, Reversal, Shipping, BalanceAffecting, ECheck
        'amt' => '', // Search by transaction amount.
        'currencycode' => '', // Search by currency code.
        'status' => '', // Search by transaction status.  Possible values: Pending, Processing, Success, Denied, Reversed
        'profileid' => ''       // Recurring Payments profile ID.  Currently undocumented but has tested to work.
    );

    $PayerName = array(
        'salutation' => '', // Search by payer's salutation.
        'firstname' => '', // Search by payer's first name.
        'middlename' => '', // Search by payer's middle name.
        'lastname' => '', // Search by payer's last name.
        'suffix' => ''         // Search by payer's suffix.
    );


    $PayPalConfig = array(
        'Sandbox' => $sandbox,
        'APIUsername' => $api_username,
        'APIPassword' => $api_password,
        'APISignature' => $api_signature,
        'PrintHeaders' => $print_headers,
        'LogResults' => $log_results,
        'LogPath' => $log_path,
    );

    $PayPal = new angelleye\PayPal\PayPal($PayPalConfig);
    $PayPalRequest = array(
        'TSFields' => $TSFields,
        'PayerName' => $PayerName
    );

    $transaction = $PayPal->TransactionSearch($PayPalRequest);

    foreach ($transaction['SEARCHRESULTS'] as $result) {
        if (is_subscription($result['L_TRANSACTIONID']) && $result['L_STATUS'] == 'Created') {
            $record = ORM::for_table('customer')->where('customer_id', $result['L_TRANSACTIONID'])->find_one();
            if (!isset($record->customer_id)) {
                $customer = ORM::for_table('customer')->create();
                $customer->customer_id = $result['L_TRANSACTIONID'];
                $customer->customer_email = $result['L_EMAIL'];
                $customer->customer_name = $result['L_NAME'];
                $customer->customer_created = gmdate("Y-m-d H:i:s", strtotime($result['L_TIMESTAMP']));
                $customer->customer_status = $result['L_STATUS'];
                $customer->save();
            }
        }
    }
}


$records = ORM::for_table('customer')->find_many();

foreach ($records as $customer) {
    $TSFields = array(
        'startdate' => gmdate("Y-m-d\\TH:i:sZ", strtotime($start_date)), // Required.  The earliest transaction date you want returned.  Must be in UTC/GMT format.  2008-08-30T05:00:00.00Z
        'enddate' => gmdate("Y-m-d\\TH:i:sZ", strtotime(date('Y-m-d'))), // The latest transaction date you want to be included.
        'email' => '', // Search by the buyer's email address.
        'receiver' => '', // Search by the receiver's email address.
        'receiptid' => '', // Search by the PayPal account optional receipt ID.
        'transactionid' => '', // Search by the PayPal transaction ID.
        'invnum' => '', // Search by your custom invoice or tracking number.
        'acct' => '', // Search by a credit card number, as set by you in your original transaction.
        'auctionitemnumber' => '', // Search by auction item number.
        'transactionclass' => 'Subscription', // Search by classification of transaction.  Possible values are: All, Sent, Received, MassPay, MoneyRequest, FundsAdded, FundsWithdrawn, Referral, Fee, Subscription, Dividend, Billpay, Refund, CurrencyConversions, BalanceTransfer, Reversal, Shipping, BalanceAffecting, ECheck
        'amt' => '', // Search by transaction amount.
        'currencycode' => '', // Search by currency code.
        'status' => '', // Search by transaction status.  Possible values: Pending, Processing, Success, Denied, Reversed
        'profileid' => $customer->customer_id       // Recurring Payments profile ID.  Currently undocumented but has tested to work.
    );

    $PayerName = array(
        'salutation' => '', // Search by payer's salutation.
        'firstname' => '', // Search by payer's first name.
        'middlename' => '', // Search by payer's middle name.
        'lastname' => '', // Search by payer's last name.
        'suffix' => ''         // Search by payer's suffix.
    );


    $PayPalConfig = array(
        'Sandbox' => $sandbox,
        'APIUsername' => $api_username,
        'APIPassword' => $api_password,
        'APISignature' => $api_signature,
        'PrintHeaders' => $print_headers,
        'LogResults' => $log_results,
        'LogPath' => $log_path,
    );

    $PayPal = new angelleye\PayPal\PayPal($PayPalConfig);
    $PayPalRequest = array(
        'TSFields' => $TSFields,
        'PayerName' => $PayerName
    );

    $transaction = $PayPal->TransactionSearch($PayPalRequest);

    $customer_email = '';

    foreach ($transaction['SEARCHRESULTS'] as $result) {
        $record = ORM::for_table('payment')->where('payment_id', $result['L_TRANSACTIONID'])->find_one();
        if ($result['L_STATUS'] == 'Completed') {
            if (!isset($record->payment_id)) {
                $customer_email = $result['L_EMAIL'];
                $payment = ORM::for_table('payment')->create();
                $payment->payment_id = $result['L_TRANSACTIONID'];
                $payment->customer_id = $customer->customer_id;
                $payment->payment_time = gmdate("Y-m-d H:i:s", strtotime($result['L_TIMESTAMP']));
                $payment->payment_amount = $result['L_AMT'];
                $payment->payment_cost = $result['L_FEEAMT'];
                $payment->payment_total = $result['L_NAME'];
                $payment->payment_status = $result['L_STATUS'];
                $payment->save();
            }
        }
    }

    $person = ORM::for_table('customer')->find_one($customer->customer_id);
    $person->set('customer_email', $customer_email);
    $person->save();
}


$records = ORM::for_table('payment')->find_many();

$date_data = [];
foreach ($records as $payment) {
    if (!isset($date_data[$payment->customer_id])) {
        $date_data[$payment->customer_id] = [];
    }
    array_push($date_data[$payment->customer_id], $payment->payment_time);
}

foreach ($date_data as $key => $value) {
    $max = max(array_map('strtotime', $value));
    $person = ORM::for_table('customer')->find_one($key);
    $person->set('customer_last_payment', date('Y-m-j H:i:s', $max));
    $person->save();
}
