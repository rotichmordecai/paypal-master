# PayPal - Users with recurring Payments

## Server Requirements

- PHP version 5.3.0 or newer.
- cURL
- MySQL

## Installation

## MySQL

- Create database 'paypal_master'
- Execute the script 'paypal_master.sql'
- You can change the default database access at lib/database.php although the default database username: 'root' and password is empty

## PayPal

- PayPal configuration can be set at config/config.php
- Fill in the sandbox domain and live domain:

$domain = $sandbox ? 'http://localhost/paypal-master/' : 'http://www.domain.com/';

- Fill in the Application ID for both Sandbox and live application :

$application_id = $sandbox ? 'APP-80W284485P519543T' : '';

- Fill in your paypal username, password and signature.

$api_username = $sandbox ? 'korir.mordecai-facilitator_api1.gmail.com' : 'LIVE_API_USERNAME';
$api_password = $sandbox ? '5PYD47VH7T4HRU5J' : 'LIVE_API_PASSWORD';
$api_signature = $sandbox ? 'AFcWxV21C7fd0v3bYYYRCpSSRl31AKuevmQ4LO5Q6EoMSrjKad4cSyHt' : 'LIVE_API_SIGNATURE';

Once paypal is setup, you will be required to run 'update.php', this will update the database with records of your recurring payments. It is most likely to take some time so it is advisable to run it on the command line 'php update.php' or as CRON. Once complete, the records can be displayed on index.php. 
