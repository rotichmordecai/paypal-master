<?php

/**
 * Search date range
 * 
 * @param DateTime $start_date
 * @param type $segment
 * @return type
 */
function date_data($start_date, $segment = 'monthly') {

    $skip_date = 30;

    switch ($segment) {
        case 'yearly':
            $skip_date = 365;
            break;
        case 'quarterly':
            $skip_date = 90;
            break;
        case 'monthly':
            $skip_date = 30;
            break;
        case 'weekly':
            $skip_date = 7;
            break;
        default:
            break;
    }

    // Today
    $current = date('Y-m-d');


    $start_date = new DateTime($start_date);
    $current = new DateTime($current);

    $temp_date = [];

    for ($i = $start_date; $i <= $current; $i->modify("+$skip_date day")) {
        $temp_date[] = $i->format("Y-m-d");
    }

    $return = [];
    for ($i = 0; $i < count($temp_date); $i++) {
        $return[] = ['date1' => $temp_date[$i], 'date2' => (isset($temp_date[$i + 1]) ? $temp_date[$i + 1] : date('Y-m-d'))];
    }

    return $return;
}

/**
 * Is transaction new subscription
 * 
 * @param type $input
 * @return boolean
 */
function is_subscription($input) {

    $re1 = '(I)'; # Any Single Character 1
    $re2 = '(-)'; # Any Single Character 2
    $re3 = '((?:[a-z][a-z]*[0-9]+[a-z0-9]*))'; # Alphanum 1

    if ($c = preg_match_all("/" . $re1 . $re2 . $re3 . "/is", $input, $matches)) {
       return TRUE;
    }
    return FALSE;
}
