<?php

require_once 'idiorm.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of database
 *
 * @author Admin
 */
class Database {

    function __construct() {
        ORM::configure('mysql:host=localhost;dbname=paypal_master');
        ORM::configure('username', 'root');
        ORM::configure('password', '');

        ORM::configure('id_column_overrides', array(
            'customer' => 'customer_id',
            'payment' => 'payment_id',
        ));
    }

}
